package Controller;


import HashTable.*;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Nico Salvai
 */
public class Controller {
    //Tabla a usar
    private TSB_OAHashtable<String,Integer> table;
    private LinkedList<String> files;
    private TSB_OAHashtableWriter writer;
    
    /**
     * Contructor del Controller, decide si crear una tabla nueva o no (
     * generalmente porque se cargara una)
     * @param newTable - indicador para crear una tabla nueva
     */
    public Controller(boolean newTable){
        if(newTable){
            table = new TSB_OAHashtable<>();
        }
    }
    
    public Controller(){
        
        if(!this.loadTable("HashTable.dat")){            
            table = new TSB_OAHashtable<>(2000);
            /*
            File file = new File("Libros");
            if (file.isDirectory()){
                for(File f:file.listFiles()){
                    
                    //files.add(f.getName());
                    //String unNombre = f.getName();
                    //String otroNombre = f.getCanonicalPath();
                    String nombre = f.getPath();
                    this.processFile(nombre);
                    
                    
                }
            }           
            */
        }
        
    }
    
    public boolean write(){
        
        if(writer == null){
            writer = new TSB_OAHashtableWriter();
        }
        return writer.write(table);
        
    }
    
    public TSB_OAHashtable<String, Integer> getTable(){
        return table;
        
    }
    
    
    /**
     * Carga una tabla guardada en el archivo cuyo nombre es pasado por parametro
     * @param file
     * @return true si se cargo con exito y false si no se pudo cargar
     */
    public boolean loadTable(String file){
        TSB_OAHashtableReader rd = new TSB_OAHashtableReader(file);
        this.table = rd.read();
        return this.table != null;
    }
    
    
    
    /**
     * Guarda una tabla en el archivo cuyo nombre es pasado por parametro
     * @param file
     * @return true si se guardo con exito y false si no se pudo guardar
     */
    public boolean saveTable(String file){
        TSB_OAHashtableWriter rd = new TSB_OAHashtableWriter(file);
        boolean flag = rd.write(table);
        return flag;
    }    
    
          
    public boolean processFiles(List<File> files){
        
        if(files != null){
            
            for(File f: files){
                String path = f.getAbsolutePath();                
                if(!this.processFile(path)) return false;               
            }
            this.write();
            return true;
        }
        return false;
    }
     /**
     * Procesa el archivo cuya direccion es pasada por parametro
     * agregando cada palabra por separado a la tabla.
     * @param fileDirectory - directorio del archivo a procesar
     * @return
     */
    
    public boolean processFile(String fileDirectory){
        if(table == null) { return false;}
        FileReader file = new FileReader();
        String key;
        int val, i =0;
        
        file.openFile(fileDirectory);
        
        while(file.hasNextToRead()){
            //System.out.print(i);
            key = file.readNextWord();
            val = table.getOrDefault(key, 0) + 1;
            
            
            table.put(key, val);
            i++;
            //System.out.println(key);
            
        }
        return true;
    }
    
    
    /**
     * hace toString a la tabla y lo devuelve
     * @return el toString de la tabla
     */
    public String watchTable(){
        return table.toString();
    }
    
   
}
