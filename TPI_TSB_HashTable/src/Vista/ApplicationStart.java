/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package Vista;

import java.net.URL;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
/**
 *
 * @author mati_
 */
public class ApplicationStart extends Application {
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        
        URL urlEscena1 = getClass().getResource("Scene.fxml");
        Parent root = FXMLLoader.load(urlEscena1);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setTitle("TSB Open Addressing HashTable");
        primaryStage.show();
        
    }
    
    
    
    
}
