/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import HashTable.*;
import Controller.*;
import java.io.File;
import java.net.URL;
import java.nio.file.DirectoryStream.Filter;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * FXML Controller class
 *
 * @author mati_
 */
public class SceneController implements Initializable {

    @FXML
    private Label lblCantPalabras;
    @FXML
    private TextField txtPalabraBuscar;
    @FXML
    private Button btnBuscarPalabra;
    @FXML
    private Label lvlCantApariciones;
    @FXML
    private Button btnFileChooser;
    

    private Controller controller;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {                 
        //TODO
        controller = new Controller();
        String cantPalabras = ((Integer)controller.getTable().size()).toString();
        
        lblCantPalabras.setText(cantPalabras);
       
        
    }    

    @FXML
    private void botonBuscarClick(ActionEvent event) {
        
        String key = txtPalabraBuscar.getText();
        Integer value = controller.getTable().getOrDefault(key, 0);
        lvlCantApariciones.setText(value.toString());
        
        
    }

    @FXML
    private void botonFileChooserClick(ActionEvent event) {
        
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new ExtensionFilter("Archivos de texto", "*.txt"));
        fc.setInitialDirectory(new File("").getAbsoluteFile());
        List<File> files = fc.showOpenMultipleDialog(null);     
        
        if(!controller.processFiles(files)){
            
            Alert a = new Alert(Alert.AlertType.WARNING);            
            a.setHeaderText("Ocurrió un error al procesar uno o más de los archivos seleccionados");
            a.show();
        }
        
        String cant;
        cant = ((Integer)controller.getTable().size()).toString();
        lblCantPalabras.setText(cant);     
        
    }

}
