/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import HashTable.TSB_OAHashtable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mati_
 */
public class Test_TSB_OAHashTable {
    TSB_OAHashtable<String, Integer> tabla;
    
    public Test_TSB_OAHashTable() {
    }
    
    
    
    @Before
    public void setUp() {
        
        tabla = new TSB_OAHashtable<>();
       
        String palabras[] = {"hola", "a", "todos",  "hola", "lala", "hola", "estoas", "son", "palabras",
        "de", "prueba", "para", "probar", "el rehash", "y los demas"};
        for (String string : palabras) {             
             Integer v = tabla.get(string);
             
             if(v != null){
                 v++;
             } else{
                 v = 1;
             }     
             
            tabla.put(string, v);
        }
    }
    
    @Test
    public void testPut(){
        String palabras[] = {"hola", "a", "todos", "hola", "lala", "hola", "estoas", "son", "palabras",
        "de", "prueba", "para", "probar", "el rehash", "y los demas"};
        for (String string : palabras) {             
             Integer v = tabla.get(string);
             
             if(v != null){
                 v++;
             } else{
                 v = 1;
             }     
             
            tabla.put(string, v);
            
        }
        System.out.println(tabla);
        
    }
    
    @Test
    public void testContains(){
        
        assertEquals(3, (int)tabla.get("hola"));
        assertEquals(true, tabla.containsValue(3));
    }
    
    @Test
    public void testRemove(){
        
        int res = tabla.remove("hola");
        assertEquals(3, res);
        assertEquals(null, tabla.remove("hola"));
        
    }
    
    @Test
    public void testValueCollection(){
        Collection<Integer> col = tabla.values();
        assertEquals(col.size(), tabla.size());
        System.out.println("TEST VALLUECOLLECTION");
        System.out.println(tabla);
        Iterator<Integer> it = col.iterator();
        System.out.println("--------------");
        while(it.hasNext()){
            Integer v = it.next();
            System.out.println(v);
            if(v > 1){
                it.remove();
            }
        }
        System.out.println("------------");
        it = col.iterator();
        
        while(it.hasNext()){
            Integer v = it.next();
            System.out.println(v);
            
        }
        System.out.println("--------------");
        System.out.println(tabla);
    }
    
    @Test
    public void testEntrySet(){
        
        Set<Map.Entry<String,Integer>> entrySet = tabla.entrySet();
        assertEquals(entrySet.size(), tabla.size());
        int tam = tabla.size();
        System.out.println("TEST ENTRYSET");
        System.out.println(tabla);
        Iterator<Map.Entry<String,Integer>> it = entrySet.iterator();
        
        System.out.println("----------------");
        while(it.hasNext()){
            Map.Entry<String, Integer> entry = it.next();
            System.out.println(entry.getKey() + "  " + entry.getValue());
            if(entry.getValue() > 1){
                it.remove();
            }            
        }
        System.out.println("----------------");
        it = entrySet.iterator();
        while(it.hasNext()){
            Map.Entry<String, Integer> entry = it.next();
            System.out.println(entry.getKey() + "  " + entry.getValue());
                        
        }
        assertEquals(tam - 1, tabla.size());
                
        
        
    }
    
    @Test
    public void testKeySet(){
        
        Set<String> keySet = tabla.keySet();
        assertEquals(keySet.size(), tabla.size());
        int tam = tabla.size();
        System.out.println("TEST ENTRYSET");
        System.out.println(tabla);
        Iterator<String> it = keySet.iterator();
        
        System.out.println("----------------");
        while(it.hasNext()){
            String key = it.next();
            System.out.println(key);
            if(key == "hola" || key == "cacas"){
                it.remove();
            }            
        }
        System.out.println("----------------");
        it = keySet.iterator();
        while(it.hasNext()){
            String key = it.next();
            System.out.println(key);
                        
        }
        assertEquals(tam - 2, tabla.size());
                
        
        
    }
    
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
